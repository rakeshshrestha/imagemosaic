function H = DLT(x, xp)
    %% @params x, xp: corresponding points in two images
    %% @output H: estimated homography
    
	% p stands for prime(', not transpose!) ie xp means x'
	if (size(x) ~= size(xp))
		error('dimensions of corresponding points must be same');
	end
	
	if(size(x,1) ~= 3)
		error('points must be in homogeneous coordinates [x; y; w]');
    end
    
    if(size(x,2) < 4)
        error('need at least 4 points for homography estimation');
    end
    
    x = bsxfun(@rdivide, x, x(3,:));
    xp = bsxfun(@rdivide, xp, xp(3,:));
	
	n = size(x,2);
    
    %% using only two rows (as w not equals to 0)
	A = zeros(2*n, 9);
	for i=1:size(xp,2)
		xi = x(:,i);
		xip = xp(:,i);
		A(i*2-1:i*2, :) = [...
							zeros(1,3)	-xip(3)*xi'	xip(2)*xi'
							xip(3)*xi'	zeros(1,3)	-xip(1)*xi'
							% -xip(2)*xi'	xip(1)*xi'	zeros(1,3)
						];
	end
	[~,~,V] = svd(A);
	h = V(:, end);
	H = reshape(h,3,3)';
    H = H ./ H(9); % get the correct scaling
	
	%% using all three rows
% 	A = zeros(3*n, 9);
% 	for i=1:size(xp,2)
% 		xi = x(:,i);
% 		xip = xp(:,i);
% 		A(i*3-2:i*3, :) = [...
% 							zeros(1,3)	-xip(3)*xi'	xip(2)*xi'
% 							xip(3)*xi'	zeros(1,3)	-xip(1)*xi'
% 							-xip(2)*xi'	xip(1)*xi'	zeros(1,3)
% 						];
% 	end
% 	[~,~,V] = svd(A);
% 	h = V(:, end);
%     H = reshape(h,3,3)';
	
	
	


	%% for debugging
	% disp(rank(A));
	
	
end
