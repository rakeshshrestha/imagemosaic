function [inliers]  = RANSAC(fa, fb)
	%% @params fa, fb: corresponding points in homogeneous coordinages
	%% @output inliers: columns containing inliers
	
	if(size(fa) ~= size(fb))
		error('size of corresponding feature points must be same');
	end

	if(size(fa,1)~=3)
		error('each feature point should be in the format [wx; wy; w]');
	end

	% start with infinte number of iterations, and reduce iterations based on proportion of inliers and outliers
	N = inf;
	% assume we get 50% outliers every time
	e = .5;
	% we take four samples in each iteration 
	s = 4; 
	% threshold (each coordinate of homographic mapping is allowed to stray by this amount only)
	T = .05; 
	% confidence probability (estimated probability of inliers in the set)
	p = .9;

	inliers = [];
	
	sampleCount = 0;
	n = size(fa,2);

	while(N>sampleCount)
		% choose s corresponding feature points in random
		samplePoints = randperm(n,s);
        % find DLT of the s points
		H = DLT(fa(:, samplePoints), fb(:, samplePoints));
		% find inlier among feature points (within threshold T)
		fa_p = H * fa;
		% convert [wx,wy,w] to [x,y,1]
		fa_p = bsxfun(@rdivide, fa_p, fa_p(3,:)); % fa_p = fa_p ./ fa_p(3,:);
		difference = abs(fb(1:2,:) - fa_p(1:2,:));
		thresholdedDifference = difference < T;
		currentInliers = find(thresholdedDifference(1,:) & thresholdedDifference(2,:));
		
		e = 1 - length(currentInliers)/n;
		N = log(1-p)/log(1-((1-e)^s));
		if (length(currentInliers) > length(inliers))
			inliers = currentInliers;
		end

		sampleCount = sampleCount + 1;

		%% for debug
		%disp(sprintf('%d/%d, N=%f, e=%f\n',length(currentInliers), n, N, e));
	end

end