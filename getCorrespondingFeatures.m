function [fa, fb] = getCorrespondingFeatures(Ia, Ib)
	% --------------------------------------------------------------------
	%                                           Extract features and match
	% --------------------------------------------------------------------
	[fa,da] = vl_sift(im2single(rgb2gray(Ia))) ;
	[fb,db] = vl_sift(im2single(rgb2gray(Ib))) ;
	
	[matches, scores] = vl_ubcmatch(da,db) ;

	[drop, perm] = sort(scores, 'descend') ;
	matches = matches(:, perm) ;
	scores  = scores(perm) ;
	
	% get the x and y coordinates of matching features
	fa = fa(1:2, matches(1,:)) ;
	fb = fb(1:2, matches(2,:)) ;
		
end
	
	
