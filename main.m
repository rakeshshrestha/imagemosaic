close all;
clear all;
% --------------------------------------------------------------------
%                                                    Create image pair
% --------------------------------------------------------------------

% note: format of image: x,y (width X height) is different from format of matrix (height X width)

% Im{1} = imread('shanghai/shanghai01.jpg');
% Im{2} = imread('shanghai/shanghai02.jpg');
% Im{3} = imread('shanghai/shanghai03.jpg');

Im{1} = imread('apron/ApronC08.JPG');
Im{2} = imread('apron/ApronC09.JPG');
Im{3} = imread('apron/ApronC10.JPG');
Im{4} = imread('apron/ApronC11.JPG');
Im{5} = imread('apron/ApronC12.JPG');


% Im{1} = imread(fullfile(vl_root, 'data', 'roofs1.jpg')); 
% Im{2} = imread(fullfile(vl_root, 'data', 'roofs2.jpg')); 

% Im{1} = imread(fullfile(vl_root, 'data', 'river1.jpg')); 
% Im{2} = imread(fullfile(vl_root, 'data', 'river2.jpg')); 

% resize image (because I am running out of memory)
for i = 1:length(Im)
	Im{i} = imresize(Im{i},.5);
end

% transformation for normalizing pixel co-ordinates, lower left is (-1,1), upper right is (1,1)
% linear transformation for x: when x=1, x'=-1; when x=w, x'=1
% linear transformation for y: when y=1, y'=1; when y=h, y'=-1

T{1} = [...
			2/(size(Im{1}, 2)-1)	0	-1-2/(size(Im{1}, 2)-1)
			0	-2/(size(Im{1}, 1)-1)	1+2/(size(Im{1}, 1)-1)
			0	0			1			
		];
H1 = zeros(3,3,length(Im));
H2 = zeros(3,3,length(Im));
for i=2:length(Im)
	
	T{i} = [...
			2/(size(Im{i}, 2)-1)	0	-1-2/(size(Im{i}, 2)-1)
			0	-2/(size(Im{i}, 1)-1)	1+2/(size(Im{i}, 1)-1)
			0	0			1			
		];

	% get corresponding features (in homogeneous frame)
	[fa, fb] = getCorrespondingFeatures(Im{i-1}, Im{i}) ;
	% normalize features coordinates to the range -1, 1
	fa = T{i-1} * [fa; ones(1,size(fa,2))];
	fb = T{i} * [fb; ones(1,size(fb,2))];
	% get only the inliers using RANSAC
	inliers = RANSAC(fa, fb);
	% get homography from all the inliers (mapping image1 to image2)
	H1(:,:,i-1) = DLT(fa(:,inliers), fb(:, inliers));
	% find homography with minimum reprojection error (in least square sense)
	%[H2(:,:,i-1),~,~] = computeHomographyWithLeastSquareError(fb(:, inliers), fa(:, inliers));
end
%% use the homography to align the images with reference image
% centered
panorama1 = panoramicStitchMultipleCentered(Im, T, H1);
panorama2 = panoramicStitchMultipleCentered(Im, T, H2);

%non-centered
%panorama1 = panoramicStitchMultiple(Im, T, H1);
%panorama2 = panoramicStitchMultiple(Im, T, H2);

%% for debugging
disp(sprintf('inlier/total: %d/%d',length(inliers),size(fa,2)));

% tranform back to image coordinates
fa = inv(T{length(Im)-1})*fa;
fb = inv(T{length(Im)})*fb;

%% for display purpose
% plotCorrespondence(Im{length(Im)-1}, Im{length(Im)}, fa, fb);
% figure;
% plotCorrespondence(Im{length(Im)-1}, Im{length(Im)}, fa(1:2,inliers), fb(1:2,inliers));
% figure;
imagesc(panorama1);
axis off;
figure
imagesc(panorama2);
axis off;






