function panorama = panoramicStitch(Im1, Im2, T1, T2, H)
	%% @return panoramic stich between images
	%% @param Im1, Im2: images
	%% @param T1, T2: normalization matrices of two images respectively
	%% @param H: normalized homography from Im1 to Im2

	w1 = size(Im1, 2);
	h1 = size(Im1, 1); 
	w2 = size(Im2, 2);
	h2 = size(Im2, 1); 

	% homography matrix that converts pixels in image 1 to pixels in image 2 (in image frame)
	H12 = inv(T2)*H*T1;
	H21 = inv(H12);
	% convert the corners of image 2 to image 1 coordinate frame (order: topleft, topright, bottomright, bottomleft)
	corners =  [...
					1	w2	w2	1
					1	1	h2	h2
					1	1	1	1
				];
	corners = H21 * corners;
    % convert [wx,wy,w] to [x,y,1]
	corners = bsxfun(@rdivide, corners, corners(3,:));
	xMin = floor(min([1 corners(1,:)]));
	xMax = ceil(max([w2 corners(1,:)]));
	yMin = floor(min([1 corners(2,:)]));
	yMax = ceil(max([h2 corners(2,:)]));
	[xReference,yReference] = meshgrid(xMin:xMax, yMin:yMax);
	% create reference image (with respect to second image)
	Ireference = vl_imwbackward(im2double(Im1),xReference,yReference);
	% map coordinates of reference image to second image
	w = H12(3,1) * xReference + H12(3,2) * yReference + H12(3,3);
	x = (H12(1,1) * xReference + H12(1,2) * yReference + H12(1,3)) ./ w;
	y = (H12(2,1) * xReference + H12(2,2) * yReference + H12(2,3)) ./ w;


	Im2Projection = vl_imwbackward(im2double(Im2),x,y);
	divisor = ~isnan(Ireference) + ~isnan(Im2Projection);
	Ireference(isnan(Ireference)) = 0;
	Im2Projection(isnan(Im2Projection)) = 0;
	panorama = (Ireference + Im2Projection) ./ divisor;
end
