function panorama = panoramicStitchMultipleCentered(Im, T, H)
	%% @return panoramic stich between images, with first image as reference
	%% @param Im: images cell array
	%% @param T: normalization matrices for each image
	%% @param H: normalized homography (H(:,:,i} denotes homography from I{i} to I{i+1})
	
	if(length(Im)<2)
		error('need at least two images for stiching');
	end
	
	centerIndex = floor((length(Im)+1)/2);
	%% find denormalize homographies to the first image
	% the first element is homography from 1 to reference (with is one itself)
	H_denorm = zeros(3,3,length(Im));
	H_denorm(:,:,centerIndex) = eye(3);
	
	% for images before center
	for i = centerIndex-1:-1:1
		% homography from image i to image i+1
		H_temp = inv(T{i+1})*H(:,:,i)*T{i};
		% homography from i to centerIndex
		H_denorm(:,:,i) = H_denorm(:,:,i+1) * H_temp;
	end

	% for images after center
	for i = centerIndex+1:length(Im)
		% homography from image i to image i-1
		H_temp = inv(T{i-1})*inv(H(:,:,i-1))*T{i};
		% homography from i to 1
		H_denorm(:,:,i) = H_temp * H_denorm(:,:,i-1);
	end

	% find corners of images
	corners = zeros(3,4,length(Im));
	for i = 1:length(Im)
		corners(:,:,i) = [...
						1	size(Im{i}, 2)	size(Im{i}, 2)	1
						1	1	size(Im{i}, 1)	size(Im{i}, 1)
						1	1	1	1
					];
		% to the reference frame (image 1)
		corners(:,:,i) = H_denorm(:,:,i) * corners(:,:,i);
		corners(:,:,i) = bsxfun(@rdivide, corners(:,:,i), corners(3,:,i));
	end

	xMin = floor(min(min(corners(1,:,:))));
	xMax = ceil(max(max(corners(1,:,:))));
	yMin = floor(min(min(corners(2,:,:))));
	yMax = ceil(max(max(corners(2,:,:))));
	
	[xReference,yReference] = meshgrid(xMin:xMax, yMin:yMax);

	%% interpolate images to reference image (center)
	for i=1:length(Im)
		% map coordinates of reference image to ith image
		H_refToIth = inv(H_denorm(:,:,i));
		w = H_refToIth(3,1) * xReference + H_refToIth(3,2) * yReference + H_refToIth(3,3);
		x = (H_refToIth(1,1) * xReference + H_refToIth(1,2) * yReference + H_refToIth(1,3)) ./ w;
		y = (H_refToIth(2,1) * xReference + H_refToIth(2,2) * yReference + H_refToIth(2,3)) ./ w;

		% interpolate the image to reference image frame
		I_transformed{i} = vl_imwbackward(im2double(Im{i}),x,y);
	end
	
	divisor = ~isnan(I_transformed{1});
	for i=2:length(I_transformed)
		divisor = divisor + ~isnan(I_transformed{i});
	end
	
	for i=1:length(I_transformed)
		I_transformed{i}(isnan(I_transformed{i})) = 0;
	end

	panorama = I_transformed{1};
	for i=2:length(I_transformed)
		panorama = panorama + I_transformed{i};
	end
	panorama = panorama ./ divisor;
end
