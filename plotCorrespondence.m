function  [] = plotCorrespondence(Ia, Ib, fa, fb)
	%% @params Ia, Ib: two images
	%% @params fa, fb: corresponding features
    
	imagesc(cat(2, Ia, Ib)) ;

	%% convert normalized coordinates back to image coordinates
	% xa = (fa(1,:) + 1) * (size(Ia,2) - 1) / 2 + 1 ;
	% % this coordinte is in the concatenated image
	% xb = (fb(1,:) + 1) * (size(Ib,2) - 1) / 2 + 1 + size(Ia,2) ;
	% ya = ((fa(2,:) - 1) * (size(Ia,1) - 1) / - 2) + 1 ;
	% yb = ((fb(2,:) - 1) * (size(Ib,1) - 1) / - 2) + 1 ;

	%% without converting 
	xa = fa(1,:) ;
	% this coordinte is in the concatenated image
	xb = fb(1,:) + size(Ia,2) ;
	ya = fa(2,:) ;
	yb = fb(2,:) ;

    axis off;
	hold on ;
	h = line([xa ; xb], [ya ; yb]) ;
	set(h,'linewidth', 1, 'color', 'b') ;
    boarder = line([size(Ia,2) ; size(Ia,2)], [1; size(Ia,1)]);
    set(boarder, 'linewidth', 1, 'color', 'r');

	vl_plotframe([xa; ya]) ;
	vl_plotframe([xb; yb]) ;
end