## About

This project implements an image mosaic algorithm for stitching two or more images of the same scene based on homography. To find homography between a pair of images, we first find the feature points of the two images. Then with the help of descriptors of the feature points, we find corresponding points in the images. The feature descriptor and detector that has been used is SIFT (Scale Invariant Feature Transform). VLFeat library was used for SIFT operations. 
Normalized coordinates of these feature points were used to estimate homography. We find linear transformation between homogeneous coordinates of these normalized feature points using Direct Linear Transformation (DLT). Random Sample Consensus (RANSAC) algorithm was used to find the largest set of points that fit on a global homography model. Using only the best fitting points (inliers), we determine homography between two images (in normalized pixel coordinates). With the homography known, we project the images to align with a reference image (which in our case is one of
the given images itself) and generate the required image mosaic.

## Files and Folders
main.m is the main script and other files are helper modules.The folders within this repository contain sample images for merging.

## Language
The code will work on Matlab and octave as well (if the image package is installed in your octave).

## Note
This project requires that you have VLFeat library already setup before running its scripts.
