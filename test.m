close all
graphics_toolkit('gnuplot');
% --------------------------------------------------------------------
%                                                    Create image pair
% --------------------------------------------------------------------

% note: format of image: x,y (width X height) is different from format of matrix (height X width)

% Im{1} = imread('shanghai/shanghai01.jpg');
% Im{2} = imread('shanghai/shanghai02.jpg');

% Im{1} = imread('roofs/roofs1.jpg');
% Im{2} = imread('roofs/roofs2.jpg');
 
Im{1} = imread(fullfile(vl_root, 'data', 'roofs2.jpg')); 
Im{2} = imread(fullfile(vl_root, 'data', 'roofs1.jpg')); 

% resize image (because I am running out of memory)
% Im{1} = imresize(Im{1},.5);
% Im{2} = imresize(Im{2},.5);

% transformation for normalizing pixel co-ordinates, lower left is (-1,1), upper right is (1,1)
% linear transformation for x: when x=1, x'=-1; when x=w, x'=1
% linear transformation for y: when y=1, y'=1; when y=h, y'=-1

w1 = size(Im{1},2);
h1 = size(Im{1}, 1);
T1 = [...
		2/(w1-1)	0	-1-2/(w1-1)
		0	-2/(h1-1) 1+2/(h1-1)
		0	0			1			
	];

w2 = size(Im{2},2);
h2 = size(Im{2}, 1);
T2 = [...
		2/(w2-1)	0	-1-2/(w2-1)
		0	-2/(h2-1) 1+2/(h2-1)
		0	0			1			
	];

% get corresponding features (in homogeneous frame)
[fa, fb] = getCorrespondingFeatures(Im{1}, Im{2}) ;
% normalize features coordinates to the range -1, 1
fa = T1 * [fa; ones(1,size(fa,2))];
fb = T2 * [fb; ones(1,size(fb,2))];
% get only the inliers using RANSAC
inliers = RANSAC(fa, fb);

% get sample points
%samplePoints = randperm(size(fa,2),4);%1:size(fa,2);%
samplePoints = inliers;
%samplePoints = [35 136 253 205];
% find DLT of the s points
H = DLT(fa(:, samplePoints), fb(:, samplePoints));
% find inlier among feature points (within threshold T)
fa_p = H * fa;
% convert [wx,wy,w] to [x,y,1]
fa_p = bsxfun(@rdivide, fa_p, fa_p(3,:)); % fa_p = fa_p ./ fa_p(3,:);
difference = abs(fb(1:2,:) - fa_p(1:2,:));
thresholdedDifference = difference < .05;
inliers = find(thresholdedDifference(1,:) & thresholdedDifference(2,:));


% get homography from all the inliers (mapping image1 to image2)
H1 = DLT(fa(:,inliers), fb(:, inliers));
% find homography with minimum reprojection error (in least square sense)
[H2,~,~] = computeHomographyWithLeastSquareError(fb(:, inliers), fa(:, inliers));
%% use the homography to align the images with reference image
% panorama1 = panoramicStitch(Im{1}, Im{2}, T1, T2, H1);
% panorama2 = panoramicStitch(Im{1}, Im{2}, T1, T2, H2);

%% for debugging
disp(sprintf('inlier/total: %d/%d',length(inliers),size(fa,2)));

% tranform back to image coordinates
fa = inv(T1)*fa;
fb = inv(T2)*fb;
%% for display purpose
% plotCorrespondence(Im{1}, Im{2}, fa, fb);
% figure;
plotCorrespondence(Im{1}, Im{2}, fa(1:2,inliers), fb(1:2,inliers));
figure;
plotCorrespondence(Im{1}, Im{2}, fa(1:2,samplePoints), fb(1:2,samplePoints));
figure;
plotCorrespondence(Im{1}, Im{2}, fa(1:2,:), fb(1:2,:));
% figure
% imagesc(panorama1);
% axis off;
% figure
% imagesc(panorama2);
% axis off;







